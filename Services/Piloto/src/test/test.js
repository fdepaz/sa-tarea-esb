
var chai = require('chai');
var chaiHttp = require('chai-http');
//const app = require('../index');
var should = require('should');

chai.use(chaiHttp);
before(done => {
  console.log(
    "\n-----------------------\n-- START TEST --\n-------------------------"
  );
  done();
});
after(done => {
  console.log(
    "\n-----------------------\n-- END TEST --\n-------------------------"
  );
  done();
});

describe('Prueba de sistema del piloto', () => {
  describe('Obtener index piloto ', () => {
    it('obtener index piloto', done => {
      chai
        .request('http://localhost:3001')
        .get('/')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            done();
        });
    }).timeout(0);
  });

  describe('Obtener una respuesta del piloto ', () => {
    it('obtener si el piloto acepta o no', done => {
      chai
        .request('http://localhost:3001')
        .get('/obtenerRespuesta')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            res.body.should.have.property('acepta');
            done();
        });
    }).timeout(0);
  });
});
