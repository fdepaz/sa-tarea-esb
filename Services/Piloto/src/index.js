// Se hace uso de express para levantar el servicio
const express = require("express");
// Se inicializa la instancia
const app = express();
// Variables globales a utilizar
const port = 3001;
let solicitudes = [];
solicitudes.push(1);
solicitudes = [];
// Se define el método de parseo de booleans
var boolParser = require("express-query-boolean");
app.use(boolParser());
/**
 * Método del index
 * @returns Mensaje de bienvenida
 */

app.get("/", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      msg: "Bienvenido a la aplicación de simulación de piloto"
    })
  );
});

/**
 * Se debería incluir la lógica para determinar si el piloto se encuentra
 * en el área cercana al cliente. Por lo tanto devuelve al piloto más cercano
 * y que se encuentre en estado disponible
 *
 */
app.get("/obtenerUbicacion", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify({ latitud: 1, longitud: 2, idpiloto: 301403602 }));
});

/**
 * Se retorna la respuesta brindada por el usuario luego de que este ha confirmado o rechazado
 * la solicitud.
 */
app.get("/obtenerRespuesta", function(req, res) {
  var idpiloto = req.query.idpiloto;
  var respuestaPiloto = Math.random() >= 0.5;
  console.log(
    respuestaPiloto ? "El piloto ha aceptado" : "El piloto ha rechazado"
  );
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      acepta: respuestaPiloto,
      latitud: 1,
      longitud: 2,
      idpiloto: idpiloto
    })
  );
});

/**
 * Se encarga obtener la respuesta brindada por el piloto
 * (si acepta o no el ride)
 */
app.get("/responderSolicitud", function(req, res) {
  var respuesta = req.query.respuesta;
  res.setHeader("Content-Type", "application/json");
  if (respuesta) {
    solicitudes = [];
    //cambiar el estado del piloto
    res.end(
      JSON.stringify({
        acepta: true,
        latitud: 1,
        longitud: 2,
        idpiloto: 301403602
      })
    );
  }
  // enviar respuesta a ESB
  res.end(JSON.stringify({ acepta: false }));
});

/**
 * Se notifica al piloto sobre la solicitud de destino para que proceda a responder.
 * Se simula esta parte, por medio de una cola de solicitudes que el usuario tiene
 */
app.get("/hacerSolicitud", function(req, res) {
  var latituddestino = req.query.latituddestino;
  var longituddestino = req.query.longituddestino;
  solicitudes.push(latituddestino + "," + longituddestino);

  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      msg: "Se ha informado al piloto",
      latitud: latituddestino,
      longitud: longituddestino,
      idpiloto: 301403602
    })
  );
});

// Se levanta el servicio en el puerto indicado y se retorna un mensaje a manera de bitácora
app.listen(port);
console.log(
  `Servicio de pilotos iniciado en el puerto ${port}` + solicitudes.length
);
module.exports = app;