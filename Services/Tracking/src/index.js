// Se hace uso de express para levantar el servicio
const express = require("express");
// Se inicializa la instancia
const app = express();
// Variables globales a utilizar
const ip = "127.0.0.1";
const port = 3002;
const port_esb = 3003;

// Se define el método de parseo de booleans
var boolParser = require("express-query-boolean");
app.use(boolParser());

const request = require("request");

/**
 * Método del index
 * @returns Mensaje de bienvenida
 */

app.get("/", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      msg: "Bienvenido a la aplicación de simulación de tracking"
    })
  );
});

app.get("/buscarPiloto", function(req, res) {
  var latitud = req.query.latitud;
  var longitud = req.query.longitud;
  obtenerUbicacionPiloto(latitud, longitud, res);
});

function obtenerUbicacionPiloto(latitud, longitud, resp) {
  new Promise((resolve, reject) => {
    request(
      `http://${ip}:${port_esb}/buscarPiloto?latitud=${latitud}&longitud=${longitud}`,
      { json: true },
      (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      }
    );
  }).then(result => {
    console.log(result);
    var respuesta = JSON.stringify(result);
    var objetoRespuesta = JSON.parse(respuesta);
    if (!objetoRespuesta.idpiloto == "") {
      obtenerConfirmacion(objetoRespuesta.idpiloto, resp);
    } else {
      console.log("ERROR");
    }
  });
}

function obtenerConfirmacion(idpiloto, resp) {
  new Promise((resolve, reject) => {
    request(
      `http://${ip}:${port_esb}/obtenerRespuestaPiloto?idpiloto=${idpiloto}`,
      { json: true },
      (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      }
    );
  }).then(result => {
    resp.setHeader("Content-Type", "application/json");
    console.log(result);
    var respuesta = JSON.stringify(result);
    var objetoRespuesta = JSON.parse(respuesta);
    if (!objetoRespuesta.acepta) {
      resp.end(
        JSON.stringify({ match: false, msg: "No se ha encontrado piloto." })
      );
    } else {
      resp.end(
        JSON.stringify({
          match: true,
          idpiloto: objetoRespuesta.idpiloto,
          msg: "El piloto va en camino"
        })
      );
    }
  });
}

// Se levanta el servicio en el puerto indicado y se retorna un mensaje a manera de bitácora
app.listen(port);
console.log(`Servicio de tracking iniciado en el puerto ${port}`);
module.exports = app;