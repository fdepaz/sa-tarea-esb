
var chai = require('chai');
var chaiHttp = require('chai-http');
//const app = require('../index');
var should = require('should');

chai.use(chaiHttp);
before(done => {
  console.log(
    "\n-----------------------\n-- START TEST --\n-------------------------"
  );
  done();
});
after(done => {
  console.log(
    "\n-----------------------\n-- END TEST --\n-------------------------"
  );
  done();
});

describe('Prueba de sistema de tracking', () => {
  describe('Obtener index tracking ', () => {
    it('obtener index tracking', done => {
      chai
        .request('http://localhost:3002')
        .get('/')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            done();
        });
    }).timeout(0);
  });

  describe('Obtener una respuesta de tracking ', () => {
    it('obtener si hace match', done => {
      chai
        .request('http://localhost:3002')
        .get('/buscarPiloto')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            res.body.should.have.property('match');
            done();
        });
    }).timeout(0);
  });
});
