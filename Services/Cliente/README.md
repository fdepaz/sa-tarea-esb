# Microservicio de clientes
Directorio utilizado para almacenar el código fuente del microservicio encargado de atender las peticiones realizadas por los clientes.


## Setup de la aplicación
Para ejecutar el servicio es necesario contar con las dependencias necesarias, por lo que se procede a instalar express, a través de una terminal.

```bash
npm i express 
```

Se requiere el uso de requests para poder realizar la comunicación con el ESB.

```bash
npm install --save request
```

## Ejecución del servicio
Ejecutar el servicio se requiere ubicarse en el directorio donde se encuentra el archivo index.js y ejecutar el siguiente comando en una terminal.

```bash
node index.js
```
Luego de esto, el servicio se quedará ejecutando de manera exitosa en el puerto 3000.