
var chai = require('chai');
var chaiHttp = require('chai-http');
//const app = require('../index');
var should = require('should');

chai.use(chaiHttp);
before(done => {
  console.log(
    "\n-----------------------\n-- START TEST --\n-------------------------"
  );
  done();
});
after(done => {
  console.log(
    "\n-----------------------\n-- END TEST --\n-------------------------"
  );
  done();
});

describe('Prueba de sistema del Cliente', () => {
  describe('Obtener index cliente ', () => {
    it('obtener index cliente', done => {
      chai
        .request('http://localhost:3000')
        .get('/')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            done();
        });
    }).timeout(0);
  });

  describe('Obtener una respuesta de solicitud ', () => {
    it('obtener si hizo match o no', done => {
      chai
        .request('http://localhost:3000')
        .get('/solicitaRide')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            res.body.should.have.property('match');
            done();
        });
    }).timeout(0);
  });
});
