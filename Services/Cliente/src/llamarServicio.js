const request = require("request");

module.exports = {
  /*
   ** Se encarga de procesar el Promise y resuelve a partir de lo que retorna el API
   */
  llamarAPI: function(url, idcliente, labeldestino, latitud, longitud) {
    var final_url =
      url +
      "?idcliente=" +
      idcliente +
      "&labeldestino=" +
      labeldestino +
      "&latitud=" +
      latitud +
      "&longitud=" +
      longitud;

    return new Promise((resolve, reject) => {
      request(final_url, { json: true }, (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      });
      setTimeout(function() {
        reject("Ocurrió un error en el servicio, por favor intente más tarde");
      }, 10000);
    });
  }
};
