// Se hace uso de express para levantar el servicio
const express = require("express");
// Se inicializa la instancia
const app = express();
// Variables globales a utilizar
const ip = "127.0.0.1";
const port = 3000;
const port_esb = 3003;
// Se usa requests para permitir la comunicación con el ESB
const api_esb = require("./llamarServicio");

/**
 * Método del index
 * @returns Mensaje de bienvenida
 */

app.get("/", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      msg: "Bienvenido a la aplicación de simulación de cliente"
    })
  );
});

/**
 * Método que maneja la solicitud de un ride por parte del cliente
 * @param {Number} idcliente
 * @param {String} labeldestino
 * @param {Number} latitud
 * @param {Number} longitud
 * @returns json, determina si se encuentra piloto o no.
 */
app.get("/solicitaRide", function(req, res) {
  var idcliente = req.query.idcliente;
  var labeldestino = req.query.labeldestino;
  var latitud = req.query.latitud;
  var longitud = req.query.longitud;
  api_esb
    .llamarAPI(
      `http://${ip}:${port_esb}/solicitaRide`,
      idcliente,
      labeldestino,
      latitud,
      longitud
    )
    .then(response => {
      res.json(response);
    })
    .catch(error => {
      res.send(error);
    });
});

app.get("/obtenerRespuesta", function(req, res) {
  var respuesta = req.query.respuesta;
  var idpiloto = req.query.idpiloto;
  res.setHeader("Content-Type", "application/json");

  if (respuesta) {
    res.end(
      JSON.stringify({ msg: "El piloto está en camino", idpiloto: idpiloto })
    );
  } else {
    res.end(
      JSON.stringify({ msg: "No se ha encontrado un piloto disponible" })
    );
  }
});

app.get("/setRespuesta", function(req, res) {
  // acá se debería registrar de manera "persistente" que el piloto
  // ha aceptado la respuesta
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify({ msg: "Se ha registrado la respuesta del piloto" }));
});

// Se levanta el servicio en el puerto indicado y se retorna un mensaje a manera de bitácora
app.listen(port);
console.log(`Servicio de clientes iniciado en el puerto ${port}`);
module.exports = app;