// Se hace uso de express para levantar el servicio
const express = require("express");
// Se inicializa la instancia
const app = express();
// Variables globales a utilizar
const ip = "127.0.0.1";
const port = 3003;
const port_piloto = 3001;
const port_tracking = 3002;

// Se define el método de parseo de booleans
var boolParser = require("express-query-boolean");
app.use(boolParser());

const request = require("request");

/**
 * Método del index
 * @returns Mensaje de bienvenida
 */

app.get("/", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({ msg: "Bienvenido a la aplicación de simulación de ESB" })
  );
});

app.get("/solicitaRide", function(req, res) {
  var idcliente = req.query.idcliente;
  var labeldestino = req.query.labeldestino;
  var latitud = req.query.latitud;
  var longitud = req.query.longitud;
  resolverTracking(idcliente, latitud, longitud, labeldestino, res);
});

function resolverTracking(idcliente, latitud, longitud, labeldestino, res) {
  new Promise((resolve, reject) => {
    request(
      `http://${ip}:${port_tracking}/buscarPiloto?latitud=${latitud}&longitud=${longitud}`,
      { json: true },
      (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      }
    );
  }).then(result => {
    console.log(result);
    res.json(result);
  });
}

app.get("/buscarPiloto", function(req, res) {
  //contactar con el servicio de piloto
  var latitud = req.query.latitud;
  var longitud = req.query.longitud;
  obtenerUbicacionPiloto(latitud, longitud, res);
});

app.get("/obtenerRespuestaPiloto", function(req, res) {
  //contactar con el servicio de piloto
  var idpiloto = req.query.idpiloto;
  obtenerRespuestaPiloto(idpiloto, res);
});

function obtenerUbicacionPiloto(latitud, longitud, res) {
  new Promise((resolve, reject) => {
    request(
      `http://${ip}:${port_piloto}/obtenerUbicacion?latituddestino=${latitud}&longituddestino=${longitud}`,
      { json: true },
      (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      }
    );
  }).then(result => {
    console.log(result);
    res.json(result);
  });
}

function obtenerRespuestaPiloto(idpiloto, res) {
  new Promise((resolve, reject) => {
    request(
      `http://${ip}:${port_piloto}/obtenerRespuesta?idpiloto=${idpiloto}`,
      { json: true },
      (err, res, body) => {
        if (err) reject(err);
        resolve(body);
      }
    );
  }).then(result => {
    console.log(result);
    res.json(result);
  });
}

// Se levanta el servicio en el puerto indicado y se retorna un mensaje a manera de bitácora
app.listen(port);
console.log(`ESB iniciado en el puerto ${port}`);
module.exports = app;