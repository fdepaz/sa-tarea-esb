
var chai = require('chai');
var chaiHttp = require('chai-http');
//const app = require('../index');
var should = require('should');

chai.use(chaiHttp);
before(done => {
  console.log(
    "\n-----------------------\n-- START TEST --\n-------------------------"
  );
  done();
});
after(done => {
  console.log(
    "\n-----------------------\n-- END TEST --\n-------------------------"
  );
  done();
});

describe('Prueba de sistema del ESB', () => {
  describe('Obtener index esb ', () => {
    it('obtener index esb', done => {
      chai
        .request('http://localhost:3003')
        .get('/')
        .end(function(err, res){
            if(err) done(err);
            should(res.status).be = 200;
            done();
        });
    }).timeout(0);
  });
});
