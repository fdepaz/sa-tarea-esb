# Simulación de servicios de carros tipo Uber
Repositorio utilizado para el control de versiones del código fuente de la Tarea 2 y 3 del curso Software Avanzado. 

El proyecto consiste en la simulación de servicios tipo Uber, a través de 3 servicios que se comunican entre sí por medio de un Enterprise Service Bus


## Arquitectura de la solución
A continuación se muestra un diagrama de la arquitectura de la solución que se implementó.

![Diagrama](arquitectura.png)

### Donde
- El servicio de _Clientes_ se utiliza para recibir la solicitud de los clientes que desean abordar una unidad.

- El servicio de _Pilotos_ se utiliza para los clientes del tipo "piloto" quienes reciben las solicitudes y deciden si las rechazan o no.

- El servicio de _Tracking_ que se utiliza para rastrear la ubicación de los pilotos y además se encarga de realizar el match entre el cliente y el piloto que ha aceptado.

- La comunicación entre estos se realiza por medio del _ESB_.

### Consideraciones

- Para la comunicación entre los servicios y el ESB se utiliza el protocolo _REST_, estandarizando las respuestas a formato _JSON_.

## Datos personales
Fabio César De Paz Vásquez
201403602
