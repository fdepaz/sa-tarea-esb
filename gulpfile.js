const gulp = require('gulp');
const zip = require('gulp-zip');
const fileindex = require('gulp-fileindex');

gulp.task('buildesb', ()=>
    gulp.src('ESB/src/*')
        .pipe(zip('esb.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('buildpiloto', ()=>
    gulp.src('Services/Piloto/src/*')
        .pipe(zip('piloto.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('buildtracking', ()=>
    gulp.src('Services/Tracking/src/*')
        .pipe(zip('tracking.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('buildcliente', ()=>
    gulp.src('Services/Cliente/src/*')
        .pipe(zip('cliente.zip'))
        .pipe(gulp.dest('dist'))
);
gulp.task('indexmove', function() {
    return gulp.src('dist/*.zip')
        .pipe(fileindex())
        .pipe(gulp.dest('./dist'));
});



gulp.task('default', gulp.series('buildesb','buildpiloto','buildtracking',
                                'buildcliente','indexmove'));

